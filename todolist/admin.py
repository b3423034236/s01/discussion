from django.contrib import admin

from .models import ToDoItem
from .models import Events

admin.site.register(ToDoItem)
admin.site.register(Events)

