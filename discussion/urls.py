from django.contrib import admin
from django.urls import include, path

# Every django always starts with the package that has the same name. In order for us to access the routes of a different package (todolist)
urlpatterns = [
    path('todolist/', include('todolist.urls')),
    path('admin/', admin.site.urls),
]
